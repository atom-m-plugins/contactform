<?php
class contactform {

    public function common($params) {
        $config = json_decode(file_get_contents(dirname(__FILE__).'/config.json'), true);

        if (isset($_POST['from_name_contact'])) {
            $name = $_POST['from_name_contact'];
            $markers['name'] = $name;
            if ($name == '') unset($name);
        }
        if (isset($_POST['from_mail_contact'])) {
            $fromail = $_POST['from_mail_contact'];
            $markers['fromail'] = $fromail;
            if ($fromail == '') unset($fromail);
        }
        if (isset($_POST['from_sub_contact'])) {
            $sub = $_POST['from_sub_contact'];
            $markers['sub'] = $sub;
            if ($sub == '') unset($sub);
        }
        if (isset($_POST['from_body_contact'])) {
            $body = $_POST['from_body_contact'];
            $markers['body'] = $body;
            if ($body == '') unset($body);
        }
        if (!isset($_SESSION['pickshablons']['status']))
            $_SESSION['pickshablons']['status'] = '';
        if (!isset($_SESSION['pickshablons']['text']) or $_SESSION['pickshablons']['text'] !== @$_POST['from_body_contact']) {
            $_SESSION['pickshablons']['status'] = '';
            if (isset($name) and isset($fromail) and isset($sub) and isset($body)) {

                if ($config['mail'] == 'auto')
                    $address = Config::read('admin_email');
                 else
                    $address = $config['mail'];

                $header = "Content-type:text/plain; charset = UTF-8\r\n";
                $header .= "From:" . h($name) . ' <' . $fromail . '>' . "\r\n";
                $header .= "Reply-To:" . h($sub);

                if (mail($address,$sub,h($body),$header)) {
                    $_SESSION['pickshablons']['status'] = 'sended';
                    $_SESSION['pickshablons']['text'] = $body;
                } else
                    $_SESSION['pickshablons']['status'] = 'error';
            } elseif (isset($name) or isset($fromail) or isset($sub) or isset($body))
                $_SESSION['pickshablons']['status'] = 'incorrect';
        }

        $disign = file_get_contents(dirname(__FILE__).'/template/index.html');

        $Viewer = new Viewer_Manager;
        $markers['error'] = $_SESSION['pickshablons']['status'];
        $disign = $Viewer->parseTemplate($disign, array('data' => $markers));

        $disign .= '</body>';
        return str_replace('</body>', $disign, $params);
    }
}
?>